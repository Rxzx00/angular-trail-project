import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  getCurrentUsers: any;
  constructor(
    private commonService:CommonService, 
    private router:Router,
    
    )
  {
  }
  ngOnInit(): void {
  }
  addUser(formObj){
    console.log(formObj)
    this.commonService.createUser(formObj).subscribe((response)=> {
    this.router.navigate['/users'];
    })
  }
  
}
