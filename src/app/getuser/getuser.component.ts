import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-getuser',
  templateUrl: './getuser.component.html',
  styleUrls: ['./getuser.component.css']
})
export class GetuserComponent implements OnInit {
  allUsers: Object;

  constructor(private commonService:CommonService){}

  ngOnInit(){
    this.getCurrentUsers() 
  }

  getCurrentUsers(){
    this.commonService.getUser().subscribe((response)=> {
      this.allUsers = response
    })
  }
}
